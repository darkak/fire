/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

const getters = {
    sidebar: state => state.app.sidebar,
    token: state => state.user.token,
    avatar: state => state.user.avatar,
    name: state => state.user.name,
    introduction: state => state.user.introduction,
    status: state => state.user.status,
    roles: state => state.user.roles,
    setting: state => state.user.setting,
    permission_routers: state => state.permission.routers,
    addRouters: state => state.permission.addRouters,
    permissionMenus: state => state.user.permissionMenus,
    menus: state => state.user.menus,
    elements: state => state.user.elements,
    adminid: state => state.user.adminid,
    username: state => state.user.username,
    site: state => state.user.site,
    tenantNo: state => state.user.tenantNo,
    planSetup: state => state.pubconfig.planSetup,
    issuperadmin: state => state.user.issuperadmin
}
export default getters

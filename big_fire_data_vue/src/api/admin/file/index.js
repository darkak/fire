/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

// 查询公告列表
export function fileMerge(param) {
  return request({
    url: '/api/admin/file/merge',
    method: 'post',
    params: param
  })
}

/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function pageList(query) {
  return request({
    url: '/api/admin/tenant/pageList',
    method: 'get',
    params: query
  })
}

export function getUnbindUser(query) {
  return request({
    url: '/api/admin/user/unbindUser',
    method: 'get',
    params: query
  })
}

export function getSiteList(query) {
  return request({
    url: '/api/admin/channel/tree',
    method: 'get',
    params: query
  })
}
// 新增站点
export function addObj(obj) {
  return request({
    url: '/api/admin/tenant/add',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/api/admin/tenant/' + id,
    method: 'get'
  })
}

export function delObj(query) {
  return request({
    url: '/api/admin/tenant/delete',
    method: 'get',
    params: query
  })
}
// 更新站点
export function putObj(obj) {
  return request({
    url: '/api/admin/tenant/update',
    method: 'post',
    data: obj
  })
}

export function updateUser(id, obj) {
  return request({
    url: '/api/admin/tenant/' + id + '/user',
    method: 'put',
    params: obj
  })
}

export function getOwner(id) {
  return request({
    url: '/api/admin/tenant/' + id + '/get',
    method: 'get'
  });
}

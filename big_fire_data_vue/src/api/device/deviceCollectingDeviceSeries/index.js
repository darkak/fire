/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function page(query) {
return request({
url: '/api/device/deviceCollectingDeviceSeries/page',
method: 'get',
params: query
})
}

export function addObj(obj) {
return request({
url: '/api/device/deviceCollectingDeviceSeries',
method: 'post',
data: obj
})
}

export function getObj(id) {
return request({
url: '/api/device/deviceCollectingDeviceSeries/' + id,
method: 'get'
})
}

export function delObj(id) {
return request({
url: '/api/device/deviceCollectingDeviceSeries/' + id,
method: 'delete'
})
}

export function putObj(id, obj) {
return request({
url: '/api/device/deviceCollectingDeviceSeries/' + id,
method: 'put',
data: obj
})
}

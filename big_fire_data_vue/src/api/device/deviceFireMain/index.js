/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

// 建筑状态数量
export function getAllStatus(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/getAllStatusCount',
    method: 'get',
    params: query
  })
}
//地图建筑物
export function getBuildingList(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/getStatusByBuild',
    method: 'get',
    params: query
  })
}
//异常记录处理

export function getUnhandleList(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/getAllAlrmAndFault',
    method: 'get',
    params: query
  })
}

//主机管理列表页
export function page(query) {
  return request({
    url: '/api/device/deviceFireMain/pageList',
    method: 'get',
    params: query
  })
}

//主机添加
export function addObj(obj) {
  return request({
    url:'/api/device/deviceFireMain/add',
    method:'post',
    data: obj
  })
}
//编辑前的查询
export function getObj(query) {
  return request({
    url: '/api/device/deviceFireMain/get',
    method: 'get',
    params: query
  })
}
//修改
export function updateObj(obj) {
  return request({
    url:'/api/device/deviceFireMain/update',
    method:'post',
    data: obj
  })
}
//删除前查询
export function deleteQuery(query) {
  return request({
    url: '/api/device/deviceFireMain/deleteQuery',
    method: 'get',
    params: query
  })
}
//删除
export function delObj(query) {
  return request({
    url: '/api/device/deviceFireMain/delete',
    method: 'get',
    params: query
  })
}

//关联设备列表
export function relationPage(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/pageList',
    method: 'get',
    params: query
  })
}
//关联设备添加传感器的查询
export function getAllInfoObj(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/selectType',
    method: 'get',
    params: query
  })
}
//关联设备添加
export function addRelation(obj) {
  return request({
    url:'/api/device/deviceFireMainSensor/add',
    method:'post',
    data: obj
  })
}

//修改关联设备
export function updateRelation(obj) {
  return request({
    url:'/api/device/deviceFireMainSensor/update',
    method:'post',
    data: obj
  })
}

//删除关联设备
export function delRelationObj(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/delete',
    method: 'get',
    params: query
  })
}
//关联设备实时数据查询
export function acQuerySensor(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/get',
    method: 'get',
    params: query
  })
}
//关联设备异常记录
export function acList(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/getAlrmBySensorId',
    method: 'get',
    params: query
  })
}
//关联设备了图表
export function acChart(obj) {
  return request({
    url: '/api/device/turingBusiness/queryMainSensorRealTime',
    method: 'post',
    data: obj
  })
}
//消防主机统计分析
export function selectCountByBuild(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/selectCountByBuildId',
    method: 'get',
    params: query
  })
}

export function CountByType(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/selectCountByType',
    method: 'get',
    params: query
  })
}

export function CountByMonth(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/selectCountByDate',
    method: 'get',
    params: query
  })
}
//隐患处理情况
export function getDefect(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/getRatioByDate',
    method: 'get',
    params: query
  })
}
//消防主机异常记录

export function getRecordList(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/pageList',
    method: 'get',
    params: query
  })
}
//处理
export function changeFireRsolve(obj) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/affirmFire',
    method: 'post',
    data: obj
  })
}
//高级查询
export function selectAlrmType(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/selectAlrmType',
    method: "get",
    params: query
  })
}
//高层级查询建筑
export function selectBuild(query) {
  return request({
    url: "/api/device/deviceBuilding/getSelected",
    method: "get",
    params: query
  });
}
// 图表
export function selectCount(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/selectCount',
    method: 'get',
    params: query
  })
}

export function selectCountChart(query) {
  return request({
    url: "/api/datahandler/deviceFireMainAbnormal/selectCountNearlyMonth",
    method: "get",
    params: query
  });
}

//
export function getAllListData(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/getAllBuildingList',
    method: 'get',
    params: query
  })
}
//消防主机平面图

export function selectStatusCount(query){
  return request({
    url: '/api/device/deviceFireMainSensor/selectStatusCount',
    method: 'get',
    params: query
  })
}
// 传感器
export function getSensorCount(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/selectCount',
    methods: 'get',
    params: query
  })
}
// 报警记录
export function getAbnormalList(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/selectAbnormal',
    methods: 'get',
    params: query
  })
}
// 报警楼层
export function floorAlarm(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/getAlrmFloor',
    method: 'get',
    params: query
  })
}
//楼层的传感器
export function getFloorSensors(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/getSensorAndFloor',
    methods: 'get',
    params: query
  })
}
// 楼层传感器状态

export function floorSensorStatus(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/getSensor',
    method: 'get',
    params: query
  })
}
//设备标记
export function getUnsignCount(query) {
  return request({
    url: '/api/device/deviceFireMainSensor/getStatusCount',
    method: 'get',
    params: query
  })
}
//平面图列表模式
export function viewByList(query){
  return request({
    url: '/api/device/deviceFireMainSensor/selectByFloorGetSensor',
    method: 'get',
    params: query
  })
}
//未启用列表
export function getNotableList(query){
  return request({
    url: '/api/device/deviceFireMainSensor/getNotEnabledSensorList',
    method: 'get',
    params: query
  })
}


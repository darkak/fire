/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

// 建筑状态数量
export function selectStatusCount(query) {
  return request({
    url: '/api/device/deviceSensor/selectStatusCount',
    method: 'get',
    params: query
  })
}

//1.5版本的设备标记
export function getUnsignCount(query) {
  return request({
    url: '/api/device/deviceSensor/getStatusCount',
    method: 'get',
    params: query
  })
}
// 报警楼层
export function floorAlarm(query) {
  return request({
    url: '/api/device/deviceSensor/getAlrmFloor',
    method: 'get',
    params: query
  })
}

// 获取建筑楼层

export function getFloor(query) {
  return request({
    url: '/api/device/deviceBuilding/getFloor',
    method: 'get',
    params: query
  })
}

// 楼层传感器状态

export function floorSensorStatus(query) {
  return request({
    url: '/api/device/deviceSensor/getSensor',
    method: 'get',
    params: query
  })
}

// 传感器类别
export function getSensorCount(query) {
  return request({
      url: '/api/device/deviceSensor/selectCount',
      methods: 'get',
      params: query
  })
}

// 报警记录
export function getAbnormalList(query) {
  return request({
      url: '/api/datahandler/deviceAbnormal/selectAbnormal',
      methods: 'get',
      params: query
  })
}
// 建筑信息
export function getBuildInfo(query) {
  return request({
      url: '/api/device/deviceBuilding/selectByBuildId',
      methods: 'get',
      params: query
  })
}

//楼层的传感器
export function getFloorSensors(query) {
  return request({
      url: '/api/device/deviceSensor/getSensorAndFloor',
      methods: 'get',
      params: query
  })
}
//未启用列表
export function getNotableList(query) {
  return request({
    url: '/api/device/deviceSensor/getNotEnabledSensorList',
    methods: 'get',
    params: query
  })
}

//报警楼层类型
export function selectAlrmSensorInfo(query) {
  return request({
    url: '/api/datahandler/deviceAbnormal/selectAlrmType',
    method: 'get',
    data: query
  })
}

export function page(query) {
  return request({
    url: '/api/datahandler/deviceAbnormal/page',
    method: 'get',
    params: query
  })
}
export function pageList(query) {
  return request({
    url: '/api/datahandler/deviceAbnormal/pageList',
    method: 'get',
    params: query
  })
}

//平面图列表模式
export function viewByList(query){
  return request({
    url: '/api/device/deviceSensor/selectByFloorGetSensor',
    method: 'get',
    params: query
  })
}

// 消防给水名称位置
export function getHydrantPos(query) {
  return request({
    url: '/api/device/deviceHardwareFacilities/getStatusById',
    methods: 'get',
    params: query
  })
}
// 消防给水建筑信息
export function getHydrantBuildInfo(query) {
  return request({
    url: '/api/device/deviceHardwareFacilities/selectUnitById',
    methods: 'get',
    params: query
  })
}
// 消防给水报警记录
export function getHydrantAbnormalList(query) {
  return request({
      url: '/api/datahandler/deviceFacilitiesAbnormal/selectByHydrantId',
      methods: 'get',
      params: query
  })
}

// 消防给水出水口信息
export function getHydrantOutletInfo(query) {
  return request({
      url: '/api/device/deviceHardwareFacilities/getById',
      methods: 'get',
      params: query
  })
}
// 消防给水实时监控信息
export function getHydrantMonitorInfo(query) {
  return request({
      url: '/api/device/deviceSensor/selectByHydrantId',
      methods: 'get',
      params: query
  })
}

//消防用水详情列表模式接口
export function getHydrantDeviceList(query) {
  return request({
    url: '/api/device/deviceSensor/selectByHydrantIdGetSensor',
    methods: 'get',
    params: query
  })
}

//消防用水未启用数量
export function getHyUnsignCount(query) {
  return request({
    url: '/api/device/deviceSensor/getNotEnabledSensorListByHydrantId',
    methods: 'get',
    params: query
  })
}
//消防用水设备下的设备
export function getHyMapList(query) {
  return request({
    url: '/api/device/deviceSensor/getByHydrantIdGetSensor',
    methods: 'get',
    params: query
  })
}

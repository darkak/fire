/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function page(query) {
    return request({
    url: '/api/device/deviceAlarmThreshold/pageList',
    method: 'get',
    params: query
    })
}

export function addObj(obj) {
    return request({
    url: '/api/device/deviceAlarmThreshold/add',
    method: 'post',
    data: obj
    })
}

export function getObj(id) {
    return request({
    url: '/api/device/deviceAlarmThreshold/getUpdate?id=' + id,
    method: 'get'
    })
}

export function queryDailog() {
    return request({
        url: '/api/device/deviceAlarmThreshold/getAdd',
        method: 'get'
    })
}

export function delObj(id) {
    return request({
    url: '/api/device/deviceAlarmThreshold/delete?id=' + id,
    method: 'get'
    })
}
export function putObj(obj) {
    return request({
    url: '/api/device/deviceAlarmThreshold/update',
    method: 'post',
    data: obj
    })
}

/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function fetchTree(query) {
    return request({
        url: '/api/device/deviceBuilding/select',
        method: 'get',
        params: query
    })
}

export function advanceSelected(query) {
    return request({
        url:'/api/device/deviceSensor/getSelected',
        method:'get',
        params: query
    })
}

export function firstBuild(query) {
    return request({
        url: '/api/device/deviceBuilding/selectFirst',
        method: 'get',
        params: query
    })
}

export function page(query) {
    return request({
        url: '/api/device/deviceSensor/pageList',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
return request({
url: '/api/device/deviceBuilding',
method: 'post',
data: obj
})
}

export function getObj(id,query) {
return request({
url: '/api/device/deviceBuilding/' + id,
method: 'get',
params: query
})
}

export function delObj(id) {
return request({
url: '/api/device/deviceSensor/delete?id=' + id,
method: 'get'
})
}

export function putObj(id, obj) {
return request({
url: '/api/device/deviceBuilding/' + id,
method: 'put',
data: obj
})
}

export function getrealTime(obj){
	return request({
		url: '/api/device/turingBusiness/querySensorRealTime',
		method: 'post',
		data: obj
	})
}

export function getTestType(id,query){
	return request({
	url: '/api/device/deviceSensor/getMeasuringPoint?id=' + id,
	method: 'get',
	params: query
	})
}

export function pageRecord(query) {
    return request({
        url: '/api/datahandler/deviceAbnormal/getAlrmBySensorId',
        method: 'get',
        params: query
    })
}

export function sensorInfo(query) {
    return request({
        url: '/api/device/deviceSensor/get',
        method: 'get',
        params: query
    })
}
//消火栓实时数据
export function outerdoorInfo(query) {
  return request({
    url: '/api/device/deviceSensor/getOutdoor',
    method: 'get',
    params: query
  })
}
//室外消火栓曲线
export function outerdoorChart(obj) {
  return request({
    url: '/api/device/turingBusiness/querySensorRealTimeDay',
    method: 'post',
    data: obj
  })
}
//室外消火栓列表
export function outerdoorList(query) {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getAlrmBySensorId',
    method: 'get',
    params: query
  })
}

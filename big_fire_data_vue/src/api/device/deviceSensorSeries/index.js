/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function page(query) {
    return request({
        url: '/api/device/deviceSensorType/pageList',
        method: 'get',
        params: query
    })
}
//所属系统条件查询
export function affiliated(query) {
  return request({
    url: '/api/device/deviceSensorType/getSelected',
    method: 'get',
    params: query
  })
}

//网关设备条件查询
export function gatewaySelect(query) {
  return request({
    url: '/api/device/deviceCollectingDeviceType/getSelected',
    method: 'get',
    params: query
  })
}

export function devicePage(query) {
    return request({
        url: '/api/device/deviceCollectingDeviceType/pageList',
        method: 'get',
        params: query
    })
}

export function devicePoint() {
    return request({
        url: '/api/device/deviceMeasuringPoint/all',
        method: 'get',
    })
}
//添加传感器
export function addObj(obj) {
    return request({
        url: '/api/device/deviceSensorType/add',
        method: 'post',
        data: obj
    })
}

export function deviceAddObj(obj) {
    return request({
        url: '/api/device/deviceCollectingDeviceType/add',
        method: 'post',
        data: obj
    })
}

export function deleteQuery(id) {
    return request({
        url: '/api/device/deviceSensorType/deleteQuery?id=' + id,
        method: 'get',
    })
}

export function deviceDeleteQuery(id) {
    return request({
        url: '/api/device/deviceCollectingDeviceType/deleteQuery?id=' + id,
        method: 'get',
    })
}
//传感器修改查询
export function getObj(id) {
    return request({
        url: '/api/device/deviceSensorType/get?id='+id,
        method: 'get'
    })
}

export function deviceGetObj(id) {
    return request({
        url: '/api/device/deviceCollectingDeviceType/get?id=' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/api/device/deviceSensorType/delete?id=' + id,
        method: 'get'
    })
}

export function deviceDelObj(id) {
    return request({
        url: '/api/device/deviceCollectingDeviceType/delete?id=' + id,
        method: 'get'
    })
}
//修改传感器
export function putObj(obj) {
    return request({
        url: '/api/device/deviceSensorType/update',
        method: 'post',
        data: obj
    })
}

export function devicePutObj(obj) {
    return request({
        url: '/api/device/deviceCollectingDeviceType/update',
        method: 'post',
        data: obj
    })
}

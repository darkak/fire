/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import Vue from 'vue'

import 'normalize.css/normalize.css'// A modern alternative to CSS resets

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'


import '@/styles/index.scss' // global css

// 引入vue-simple-uploader
import uploader from 'vue-simple-uploader'
Vue.use(uploader)

import App from './App'
import router from './router'
import store from './store'

import axios from 'axios';
import qs from 'qs';

import Cookies from 'vue-cookies'
Vue.use(Cookies)
Vue.prototype.$cookie = Cookies;

// event bus
let bus = new Vue()
Vue.prototype.bus = bus

Vue.prototype.clearMapstore = function () {//changeData是函数名
  localStorage.removeItem('_AMap_vectorlayer')
  localStorage.removeItem('_AMap_wgl')
  localStorage.removeItem('_AMap_sync')
  localStorage.removeItem('_AMap_raster')
  localStorage.removeItem('_AMap_overlay')
  localStorage.removeItem('_AMap_mouse')
  localStorage.removeItem('_AMap_AMap.ToolBar')
  localStorage.removeItem('_AMap_AMap.Scale')
  localStorage.removeItem('_AMap_AMap.RangingTool')
  localStorage.removeItem('_AMap_AMap.PolyEditor')
  localStorage.removeItem('_AMap_AMap.PlaceSearch')
  localStorage.removeItem('_AMap_AMap.OverView')
  localStorage.removeItem('_AMap_AMap.MouseTool')
  localStorage.removeItem('_AMap_AMap.MarkerClusterer')
  localStorage.removeItem('_AMap_AMap.MapType')
  localStorage.removeItem('_AMap_AMap.Geolocation')
  localStorage.removeItem('_AMap_AMap.CitySearch')
  localStorage.removeItem('_AMap_AMap.CircleEditor')
  localStorage.removeItem('_AMap_AMap.Autocomplete')
  localStorage.removeItem('_AMap_AMap.IndoorMap3D')
  localStorage.removeItem('_AMap_Map3D')
  localStorage.removeItem('_AMap_labelcanvas')
  localStorage.removeItem('_AMap_labelDir')
  localStorage.removeItem('_AMap_data.tileKeys')
  localStorage.removeItem('_AMap_AMap.CustomLayer')
  localStorage.removeItem('_AMap_AMap.Geocoder')
  localStorage.removeItem('_AMap_AMap.CustomLayer')
  localStorage.removeItem('_AMap_AMap.IndoorMap')
  localStorage.removeItem('_AMap_anole')
  localStorage.removeItem('_AMap_cmng')
  localStorage.removeItem('_AMap_cvector')
}

// 优化post提交
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

Vue.prototype.$axios = axios;
Vue.prototype.$qs = qs;

// 全局注册svg icon
import IconSvg from '@/components/SvgIcon'
Vue.component('icon-svg', IconSvg)
// 图片查看器
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
Vue.use(Viewer, {
  defaultOptions: {
    zIndex: 9999
  }
})
import VueAMap from 'vue-amap';
Vue.use(VueAMap);
// import { lazyAMapApiLoaderInstance } from 'vue-amap';
// 初始化vue-amap
VueAMap.initAMapApiLoader({
  // 高德的key
  key: 'd3b736ff14cc0d90171fe3dcee513255',
  // 插件集合
  plugin: ['AMap.Autocomplete',
    'AMap.CitySearch',
    'Geolocation',
    'AMap.PlaceSearch',
    'AMap.Scale',
    'AMap.MouseTool',
    'AMap.RangingTool',
    'AMap.OverView',
    'AMap.ToolBar',
    'AMap.MapType',
    'AMap.PolyEditor',
    'AMap.CircleEditor',
    'AMap.Geocoder',
    'MarkerClusterer',
    "AMap.MarkerClusterer",
    "AMap.CircleMarker"
  ],
  // 高德 sdk 版本，默认为 1.4.4
  v: '1.4.11',
  uiVersion: '1.0.11' // 版本号
});



import './icons' // icon
import './permission' // permission control
//import './mock' // simulation data

import * as filters from './filters' // global filters



Vue.use(Element, {
  size: 'medium', // set element-ui default size
})

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
  // template: '<App/>',
  // components: { App }
})

package cn.turing.firecontrol.device.alarm.read;
import cn.turing.firecontrol.device.alarm.utils.SignUtils;
import cn.turing.firecontrol.device.alarm.config.ModbusFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 通过串口解析MODBUS协议
 */
@Component
public class ModbusRtuRead {

    @Autowired
    private ModbusFactory modbusFactory;

    /**
     * 读取烟感设备数据
     * @return
     * @throws Exception
     */
    public String read() throws Exception{
        //随机-80到80的数,index从0开始，对应100
        short[] data= modbusFactory.batchReadHoldingRegisters(ModbusFactory.SLAVE_ADDRESS, 0x0B, 2);
        String str=data[0]+"."+data[1];
        return str;
    }
}
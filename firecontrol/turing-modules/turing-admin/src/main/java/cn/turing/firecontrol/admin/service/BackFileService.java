package cn.turing.firecontrol.admin.service;

import cn.turing.firecontrol.admin.entity.BackChunk;
import cn.turing.firecontrol.admin.entity.BackFilelist;

import javax.servlet.http.HttpServletResponse;

/**
 * @author tianyandong
 */
public interface BackFileService {

    /**
     *文件上传
     * @param chunk
     * @param response
     * @return
     */
    int postFileUpload(BackChunk chunk, HttpServletResponse response);

    /**
     * 文件合并
     * @param fileInfo
     * @return
     */
    int mergeFile(BackFilelist fileInfo);
}

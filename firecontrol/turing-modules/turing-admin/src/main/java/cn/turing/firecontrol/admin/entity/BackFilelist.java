package cn.turing.firecontrol.admin.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author tianyandong
 */
@Data
public class BackFilelist implements Serializable {
    /** 主键ID */
    @ApiModelProperty("${comment}")
    private Long id;

    /** 文件名 */
//    @Excel(name = "文件名")
    @ApiModelProperty("文件名")
    private String filename;

    /** 唯一标识,MD5 */
//    @Excel(name = "唯一标识,MD5")
    @ApiModelProperty("唯一标识,MD5")
    private String identifier;

    /** 链接 */
//    @Excel(name = "链接")
    @ApiModelProperty("链接")
    private String url;

    /** 本地地址 */
//    @Excel(name = "本地地址")
    @ApiModelProperty("本地地址")
    private String location;

    /** 文件总大小 */
//    @Excel(name = "文件总大小")
    @ApiModelProperty("文件总大小")
    private Long totalSize;
}

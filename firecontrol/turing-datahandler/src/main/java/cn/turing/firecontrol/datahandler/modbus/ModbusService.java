package cn.turing.firecontrol.datahandler.modbus;

import cn.turing.firecontrol.datahandler.biz.DeviceAbnormalBiz;
import cn.turing.firecontrol.datahandler.entity.DeviceAbnormal;
import cn.turing.firecontrol.datahandler.feign.IDeviceFeign;
import cn.turing.firecontrol.datahandler.mapper.DeviceFireMainAbnormalMapper;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tianyandong
 */
@Service
public class ModbusService {

    @Autowired
    private IDeviceFeign iDeviceFeign;

    @Autowired
    private DeviceAbnormalBiz deviceAbnormalBiz;

    @Autowired
    private AmqpTemplate amqpTemplate;

//    ModbusRTUOp modbusRTUOp=new ModbusRTUOp();

    public void continueRead(String status) throws Exception{
        System.out.println("方法开始");
        //1.初始化连接处理
        //虚拟设备地理信息
        DeviceSensor deviceSensor=new DeviceSensor();
        deviceSensor.setBuildingId(111);
        deviceSensor.setChannelId(1);
        deviceSensor.setSensorTypeId(1);
        deviceSensor.setCdId(111);
        deviceSensor.setFieldStatus("1");
        deviceSensor.setStatus(status);
        deviceSensor.setSensorNo("QVFTRVTMUAOP");
        deviceSensor.setFloor(1);
        deviceSensor.setPositionDescription("展厅");
        deviceSensor.setPositionSign(null);
        deviceSensor.setDelFlag("0");
        deviceSensor.setCrtUserName("admin");
        deviceSensor.setCrtUserId("1");
        deviceSensor.setCrtTime(new Date());
        deviceSensor.setUpdUserName("admin");
        deviceSensor.setUpdUserId("1");
        deviceSensor.setUpdTime(new Date());
        deviceSensor.setDepartId(null);
        deviceSensor.setTenantId("ac88ceb386aa4231b09bf472cb937c24");
        deviceSensor.setStatusTime(new Date());
        deviceSensor.setHydrantId(null);

        //创建 DeviceAbnormal
        DeviceAbnormal deviceAbnormal = new DeviceAbnormal();
        deviceAbnormal.setSensorNo("111");
        deviceAbnormal.setEquipmentType("1");
        deviceAbnormal.setFloor(1);
        deviceAbnormal.setLevel("0");
        deviceAbnormal.setAlrmDate(new Date());
        deviceAbnormal.setAlrmType("0");
        deviceAbnormal.setHandleFlag("0");
        deviceAbnormal.setCrtUserId("1");
        deviceAbnormal.setChannelId(1);
        //status为0 是故障 否则为报警
        if ("0".equals(status)){
            deviceAbnormal.setbName("故障测试");
            //报警类型 故障
            deviceAbnormal.setAlrmCategory("0");
            deviceAbnormal.setPositionDescription("故障位置");
            deviceAbnormal.setMeasuringPoint("故障测点");
        }else {
            deviceAbnormal.setbName("报警测试");
            //报警类型 报警
            deviceAbnormal.setAlrmCategory("1");
            deviceAbnormal.setPositionDescription("报警位置");
            deviceAbnormal.setMeasuringPoint("报警测点");
            //通过rabbitMQ 异步发送消息 通知前端拉响警报
            Map<String,Object> mqMap = Maps.newHashMap();
            HashMap<String, Object> map = new HashMap<>();
            map.put("alarmdate",true);
            ArrayList<Integer> userIds = new ArrayList<>();
            userIds.add(1);
            mqMap.put("userIds",userIds);
            mqMap.put("msg",map);
            amqpTemplate.convertAndSend("tmc.alarm.demo","websocket",JSON.toJSONString(mqMap));
        }
        //添加到device_abnormal 使近七天故障或报警产生变化
        deviceAbnormalBiz.insertSelective(deviceAbnormal);
        //添加到device_sensor 使近 设备实时状态统计总数变化
        iDeviceFeign.insertDeviceSensor(deviceSensor);



        //初始化
//        ModbusRTUOp.init();
//        while(true){
//            try{
//                //index从0开始，对应100
//                short[] data = ModbusRTUOp.batchReadHoldingRegisters(ModbusRTUOp.SLAVE_ADDRESS,0x0B, 1);
//                System.out.println(""+data[0]);
//                //设置Status
//                if (data[0] <= 5){
//                    //正常
//                    deviceSensor.setStatus("2");
//                } else{
//                    //报警
//                    deviceSensor.setStatus("1");
//                }
//            }catch (Exception ex){
//                //故障
//                deviceSensor.setStatus("3");
//                System.out.println(ex.getMessage());
//            }finally {
//                //插入数据库
////                deviceSensorDao.insert(deviceSensor);
//            }
//
//            //下一阶段
//            Thread.sleep(100);
//        }
    }

}

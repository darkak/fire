package cn.turing.firecontrol.datahandler.job;

import cn.turing.firecontrol.datahandler.modbus.ModbusService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author tianyandong
 */
@Component
public class BeanMethodJobHandler {

    @Autowired
    private ModbusService modbusService;


    /**
     * 定时任务1
     * @param param
     * @return
     * @throws Exception
     */
    @XxlJob("beanMethodJobHandler")
    public ReturnT<String> beanMethodJobHandler(String param) throws Exception {
        XxlJobLogger.log("bean method jobhandler running...");
        System.out.println("调用到测试设备故障方法");
        modbusService.continueRead("0");
        return ReturnT.SUCCESS;
    }

    /**
     * 定时任务2
     * @param param
     * @return
     * @throws Exception
     */
    @XxlJob("beanMethodJobHandler2")
    public ReturnT<String> beanMethodJobHandler2(String param) throws Exception {
        XxlJobLogger.log("bean method jobhandler running...");
        System.out.println("调用到测试设备报警方法");
        modbusService.continueRead("1");
        return ReturnT.SUCCESS;
    }

}

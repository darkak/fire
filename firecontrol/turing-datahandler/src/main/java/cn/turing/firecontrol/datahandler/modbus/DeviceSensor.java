package cn.turing.firecontrol.datahandler.modbus;

import lombok.Data;

import java.util.Date;

/**
 * @author tianyandong
 */
@Data
public class DeviceSensor {
    private Long id;

    private Integer buildingId;

    private Integer channelId;

    private Integer sensorTypeId;

    private Integer cdId;

    private String fieldStatus;

    private String status;

    private String sensorNo;

    private Integer floor;

    private String positionDescription;

    private String positionSign;

    private String delFlag;

    private String crtUserName;

    private String crtUserId;

    private Date crtTime;

    private String updUserName;

    private String updUserId;

    private Date updTime;

    private String departId;

    private String tenantId;

    private Date statusTime;

    private Integer hydrantId;

}

package cn.turing.firecontrol.common.util;

import org.springframework.web.bind.annotation.RequestMapping;
import weixin.popular.api.MessageAPI;
import weixin.popular.api.TokenAPI;
import weixin.popular.api.UserAPI;
import weixin.popular.bean.message.message.TextMessage;
import weixin.popular.bean.token.Token;
import weixin.popular.bean.user.FollowResult;

import java.util.Arrays;
/**
 * @author 26049
 *
 */
public class WeiXinUtil {
    private final static String appId = "wxfb95760bbe71b977";
    private final static String appSecret = "5fc9b5937cd9dd01d7b4bf8044cd0820";

    /**
     * 发送消息给微信客户端，
     * @return
     */
    public static void sendMessage(String content){

        Token token = TokenAPI.token(appId, appSecret);
        FollowResult followResult = UserAPI.userGet(token.getAccess_token(), null);
        String[] openid = followResult.getData().getOpenid();
        Arrays.asList(openid).forEach(touser -> {
            TextMessage textMessage = new TextMessage(touser,content);
            MessageAPI.messageCustomSend(token.getAccess_token(), textMessage);
        });
//        return baseResult;
    }
}

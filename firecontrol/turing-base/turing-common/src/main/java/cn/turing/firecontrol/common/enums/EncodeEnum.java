package cn.turing.firecontrol.common.enums;

/**
 * 编码方式
 *
 * @author gx
 */
public enum EncodeEnum {

    /**
     * utf编码
     */
    UTF8(1, "UTF-8"),

    /**
     * ansi编码
     */
    ANSI(2, "ANSI"),

    /**
     * gb2312编码模式
     */
    GB2312(3, "GB2312");

    /**
     * 类型
     */
    private int type;

    /**
     * 类型描述
     */
    private String encodeType;


    EncodeEnum(int type, String encodeType) {
        this.type = type;
        this.encodeType = encodeType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getEncodeType() {
        return encodeType;
    }

    public void setEncodeType(String encodeType) {
        this.encodeType = encodeType;
    }

    public static String getEncodeByType(int type) {
        for (EncodeEnum en : values()) {

            if (en.getType() == type) {
                return en.getEncodeType();
            }
        }
        return null;
    }
}

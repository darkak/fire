package cn.turing.firecontrol.device.rest;

import cn.turing.firecontrol.common.util.WeiXinUtil;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 26049
 *
 */
@RestController
@RequestMapping("weixin")
public class WeiXinController {
    /**
     * 发送消息给微信客户端，
     * @return
     */
    @RequestMapping("sendMessage")
    public void sendMessage(){

        WeiXinUtil.sendMessage("用户登录了，如果不是本人登录，请修改密码");
//        return baseResult;
    }
}
